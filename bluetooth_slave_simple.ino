//  Simple program using HM-10s: LED on. LED off
//
//
//  Pins
//  BT VCC to Arduino 5V out. 
//  BT GND to GND
//for arduino uno> altsoftserial RX - D8, altsoftserial TX - D9
//for arduino mega> altsoftserial RX - 48, altsoftserial TX - 46

 
#include <AltSoftSerial.h>
AltSoftSerial BTSerial; 
 
char c=' ';
byte LEDpin = 13;
 
void setup() 
{
   
    BTSerial.begin(9600);  
 
    pinMode(LEDpin, OUTPUT); 
    digitalWrite(LEDpin,HIGH); 

    BTSerial.println("AT+DEFAULT" );
    delay(10); 
    BTSerial.println("AT+RESET" );
    delay(10);   
}
 
void loop()
{
    // Read from the Bluetooth module and check if it is a command
    if (BTSerial.available())
    {
        c = BTSerial.read();
 
        // 49 is the ascii code for "1"
        // 48 is the ascii code for "0"
        if (c==49)   { digitalWrite(LEDpin,HIGH);   }
        if (c==48)   { digitalWrite(LEDpin,LOW);    }
    }
 
}
