//  Simple program using HM-10s: LED on. LED off
//
//
//  Pins
//  BT VCC to Arduino 5V out. 
//  BT GND to GND
//for arduino uno> altsoftserial RX - D8, altsoftserial TX - D9
//for arduino mega> altsoftserial RX - 48, altsoftserial TX - 46


 
#include <AltSoftSerial.h>
AltSoftSerial BTserial; 
 

 
void setup() 
{
    
 
    BTserial.begin(9600);  
    
    // connect to the remote Bluetooth module

    BTserial.println("AT+DEFAULT" );
    delay(1000); 
    BTserial.println("AT+RESET" );
    delay(1000);   
    BTserial.println("AT+ROLE1" );
    delay(1000);
    BTserial.println("AT+CONAD0B5C2CC8173" );
    delay(1000);
 
}
 
void loop()
{
  BTserial.println("1" );
  delay(1000);
  BTserial.println("0" );
  delay(1000);
}
