#include <AFMotor.h>

AF_DCMotor motor(1);
int sensor_pin = 40;//digital pin 40
int led = 48;

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(sensor_pin, INPUT);
  pinMode(led, OUTPUT);
  rotate_degrees(18*5, 100, 1); //degree must be divided by 18, speed between 0 and 255, way 0 or 1
}

void loop() {
  
}


void rotate_degrees(int degree, int speedd, int way){ //degree must be divided by 18, speed between 0 and 255, way 0 or 1
  int initial_value = digitalRead(sensor_pin);

  int all_changes = ((floor)(degree/18));
  int j;
  int val=1;
  
  if (way==0){
    motor.run(FORWARD);
    }else{
      motor.run(BACKWARD);
      }
  motor.setSpeed(speedd);
  int i;
  for(i = 0; i < all_changes; i = i+1){
    pulseIn(sensor_pin, !initial_value,10000000);
    digitalWrite(led,HIGH);
    delay(20);
    digitalWrite(led,LOW);
    delay(20);

  }
  
  motor.run(RELEASE);

}
